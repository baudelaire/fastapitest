from datetime import date
from pydantic import BaseModel


class UserSchema(BaseModel):
    id: int
    name: str
    last_name_father: str
    last_name_mother: str
    age: int
    birth_date: date

    class Config:
        orm_mode = True


class UserUpdateSchema(BaseModel):
    name: str
    last_name_father: str
    last_name_mother: str
    age: int
    birth_date: date

    class Config:
        orm_mode = True
