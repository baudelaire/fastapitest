from sqlalchemy import Column, Integer, String
from sqlalchemy.types import Date

from database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    last_name_father = Column(String)
    last_name_mother = Column(String)
    age = Column(Integer)
    birth_date = Column(Date)
