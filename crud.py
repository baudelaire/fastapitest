from sqlalchemy.orm import Session

import models, schemas


def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserSchema):
    db_user = models.User(
        name=user.name,
        last_name_father=user.last_name_father,
        last_name_mother=user.last_name_mother,
        age=user.age,
        birth_date=user.birth_date
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def delete_user(db: Session, user_id: int):
    try:
        db.query(models.User).filter(models.User.id == user_id).delete()
        db.commit()
    except:
        return False
    return True


def put_user(db: Session, user_id: int, user_data: {}):
    try:
        db_user = db.query(models.User).filter(
            models.User.id == user_id).update(user_data)
        db.commit()
    except:
        return False
    return db_user
